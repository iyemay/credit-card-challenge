import React from "react";
import "./styles/App.scss"
import frontal from "./images/frontal.png";

interface ICard {
    cardNumber: number;
    cardName: string;
}

class CardForm extends React.Component<any, ICard> {
    constructor(props: any) {
        super(props);
        this.state = {
            cardNumber: this.props.defaultName,
            cardName: this.props.defaultName};
        this.handleChangeNumber = this.handleChangeNumber.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
    }

    handleChangeNumber = (event: any): void => {
        this.setState({ cardNumber: event.target.value});
    }

    handleChangeName = (event: any): void => {
        this.setState({ cardName: event.target.value});
    }


    render () {
        return (
            <div className="row justify-content-center px-5 py-5 mx-5">

                <div className="d-flex flex-column py-5 px-5 mx-5 my-5 col-6 back-form">

                    <div className="d-flex justify-content-center target-background">
                        <img alt="card" src={frontal} className="align-items-stretch"/>

                    </div>

                    <form className="form-group">

                        <div>
                            <label>Card Number</label>
                            <input className="form-control" onChange={this.handleChangeNumber}/>
                        </div>

                        <div className="d-flex flex-column pt-3">
                            <label>Card Name</label>
                            <input type="text" className="form-control" onChange={this.handleChangeName}/>
                        </div>

                        <div className="d-flex flex-row p-0 pt-3">
                            <div className="col-8 p-0">
                                <label>Expiration Date</label>
                            </div>
                            <div className="col-4 p-0">
                                <label> CVV </label>
                            </div>
                        </div>

                        <div className="d-flex flex-row">
                            <div className="col-8 d-flex flex-row p-0">
                                <div className="col-6 pl-0">
                                    <select className="form-control show-tick">
                                        <option selected>Month</option>
                                        <option>01</option>
                                        <option>02</option>
                                        <option>03</option>
                                        <option>04</option>
                                        <option>05</option>
                                        <option>06</option>
                                        <option>07</option>
                                        <option>08</option>
                                        <option>09</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                    </select>
                                </div>

                                <div className="col-6 pl-1">
                                    <select className="form-control">
                                        <option selected>Year</option>
                                        <option>2019</option>
                                        <option>2020</option>
                                        <option>2021</option>
                                        <option>2022</option>
                                        <option>2023</option>
                                        <option>2024</option>
                                        <option>2025</option>
                                        <option>2026</option>
                                        <option>2027</option>
                                        <option>2028</option>
                                        <option>2029</option>
                                        <option>2030</option>
                                    </select>
                                </div>
                            </div>

                            <div className="col-4 p-0">
                                <input type="text" className="form-control"/>
                            </div>

                        </div>

                        <div className="d-flex pt-5">
                            <button type="submit" className="btn btn-submit-form form-control shadow-lg">
                                Submit
                            </button>
                        </div>

                    </form>
                    <p>{this.state.cardNumber}</p>
                    <p>{this.state.cardName}</p>
                </div>

            </div>
        );
    }
}

export default CardForm;





